let rows = $("#input_height"),
  cols = $("#input_width"),
  table = $("#pixel_canvas"),
  color = $("#colorPicker"),
  range = $(".range");
$(document).ready(function() {
  table.children().remove(),
    $(table)
      .mousedown(function(t) {
        1 === t.which &&
          $("td").bind("mouseover", function() {
            $(this).css("background-color", color.val());
          });
      })
      .mouseup(function() {
        $("td").unbind("mouseover");
      }),
    $("td").mousedown(function(t) {
      1 === t.which && $(this).css("background-color", color.val());
    }),
    table.on("click", "td", function() {
      $(this).css("background-color", color.val());
    }),
    table.on("dblclick", "td", function() {
      $(this).css("background", "none");
    }),
    range.on("change", function() {
      $("tr").css("height", $(this).val() + "px"),
        $("td").css("width", $(this).val() - 2 + "px");
    }),
    $(".btn-row-top").click(function() {
      $("tr").css("height", range.val() + "px"),
        $("td").css("width", range.val() - 2 + "px"),
        table.prepend("<tr></tr>");
      for (let t = 0; t < $("tr:nth-child(2) td").length; t++)
        table.children("tr:first").append("<td></td>");
    }),
    $(".btn-row-bottom").click(function() {
      $("tr").css("height", range.val() + "px"),
        $("td").css("width", range.val() - 2 + "px"),
        table.append("<tr></tr>");
      for (let t = 0; t < $("tr:nth-child(1) td").length; t++)
        table
          .find("tr")
          .last()
          .append("<td></td>");
    }),
    $(".btn-row-dlt-top").click(function() {
      table.children("tr:first").remove();
    }),
    $(".btn-row-dlt-bottom").click(function() {
      table.children("tr:last").remove();
    }),
    $(".btn-col-right").click(function() {
      $("td").css("width", range.val() - 2 + "px"), $("tr").append("<td></td>");
    }),
    $(".btn-col-left").click(function() {
      $("td").css("width", range.val() - 2 + "px"),
        $("tr").prepend("<td></td>");
    }),
    $(".btn-col-dlt-left").click(function() {
      $("td:last-child").remove();
    }),
    $(".btn-col-dlt-right").click(function() {
      $("td:first-child").remove();
    });

  $("#trash").on("click", function() {
    $("td").css("background-color", "#FFF6E6");
  });
  $("#reset").on("click", function() {
    $("#pixel_canvas")
      .children()
      .remove();
  });
  var t = $("#mySoundClip")[0];
  $(".pixel_canvas_sound").on("click", function() {
    t.play();
  });
  var n = $("#trashSound")[0];
  $(".trashSound").on("click", function() {
    n.play();
  });
  var e = $("#reFresh")[0];
  $(".resetSound").on("click", function() {
    e.play();
  });
  var o = $("#addBox")[0];
  $(".addSound").on("click", function() {
    o.play();
  });
  var c = $("#delBox")[0];
  $(".delSound").on("click", function() {
    c.play();
  });
  var a = $("#artBox")[0];
  $(".artSound").on("click", function() {
    a.play();
  }),
    $(function() {
      $("#draggable").draggable();
    }),
    $("#artSubmit").on("click", function(t) {
      !(function() {
        if (
          (table.children().remove(), rows.val() <= 200 && cols.val() <= 200)
        ) {
          for (let t = 0; t < rows.val(); t++) table.append("<tr></tr>");
          for (let t = 0; t < cols.val(); t++) $("tr").append("<td></td>");
        } else
          $(".warning").css({ display: "block", left: "42%", top: "45%" }),
            $(".close-warning").click(function() {
              $(".warning").css({ display: "none" });
            });
      })(),
        t.preventDefault();
    });
  let l,
    r = $("#pixel_canvas");
  var i;
  $("#draggable").on("click", function() {
    html2canvas(r, {
      onrendered: function(t) {
        l = t;
        var n = t.getContext("2d");
        (n.strokeStyle = "#79e69d"),
          n.strokeRect(0, 0, t.width, t.height),
          (n.fillStyle = "rgba(255, 255, 255, 0.01)"),
          n.fillRect(0, 0, t.width, t.height);
        var e = n.canvas
          .toDataURL("image/png", 1)
          .replace(/^data:image\/png/, "data:application/octet-stream");
        $("#save-image")
          .attr("download", "pixel.png")
          .attr("href", e),
          window.clearTimeout(i),
          (i = window.setTimeout(function() {
            $("#save-image")[0].click();
          }, 500));
      }
    });
  });
});
const ranGe = document.getElementById("r"),
  ranges = document.querySelector(".rang"),
  outputs = document.querySelector("output[for=r]");
let val = +ranGe.value;
function update() {
  let t = +ranGe.value;
  t !== val &&
    ((val = t),
    ranges.style.setProperty("--val", val),
    (outputs.textContent = val));
}
document.documentElement.classList.add("js"),
  ranGe.addEventListener("input", update, !1),
  ranGe.addEventListener("change", update, !1),
  $(function() {
    var t = $(".page-load "),
      n = $(".enter-button");
    setTimeout(function() {
      t.removeClass("content-hidden");
    }, 800),
      n.on("click", function(n) {
        n.preventDefault(), t.addClass("content-hidden").fadeOut();
      });
  });
